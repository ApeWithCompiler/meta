package api

import (
	"net/http"
  "github.com/gorilla/mux"
  "github.com/gorilla/handlers"
  "gitlab.com/ApeWithCompiler/meta/internal/api/file"
  "os"
	"time"
)

func Start() {
	fileHandler := file.CreateFileHandler(".test/test.db")
	
	router := mux.NewRouter().StrictSlash(true)
	router.Handle("/files", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.GetFilesList))).Methods("GET")
	router.Handle("/files", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.PostFiles))).Methods("POST")
	router.Handle("/files/{fileId}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.GetFile))).Methods("GET")
	router.Handle("/files/{fileId}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.DeleteFile))).Methods("DELETE")
	router.Handle("/files/{fileId}/path", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.PutFilePath))).Methods("PUT")
	router.Handle("/files/{fileId}/tags", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.GetFileTagsList))).Methods("GET")
	router.Handle("/files/{fileId}/tags", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.PostFileTag))).Methods("POST")
	router.Handle("/files/{fileId}/tags/{tagName}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.GetFileTag))).Methods("GET")
	router.Handle("/files/{fileId}/tags/{tagName}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.PutFileTagValue))).Methods("PUT")
	router.Handle("/files/{fileId}/tags/{tagName}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(fileHandler.DeleteFileTag))).Methods("DELETE")

  srv := &http.Server{
      Handler:      router,
      Addr:         "127.0.0.1:8000",
      // Good practice: enforce timeouts for servers you create!
      WriteTimeout: 15 * time.Second,
      ReadTimeout:  15 * time.Second,
  }
	srv.ListenAndServe()
}
