package file

import (
  "os"
  "gorm.io/gorm"
  "gorm.io/driver/sqlite"
)

func reduceTagIds (tags []Tag) []uint {
  var ids []uint
  for i,_ := range(tags) {
    ids = append(ids, tags[i].FileRefer)
  }
  return ids
}

func EnsureDbExists (dbfile string) {
  _, err := os.Stat(dbfile)
  if os.IsNotExist(err) {
    db, err := gorm.Open(sqlite.Open(dbfile), &gorm.Config{})
    if err != nil {
      panic("failed to connect database")
    }
    db.AutoMigrate(&Tag{})
    db.AutoMigrate(&File{})
  }    
}

func CreateFilePersistenceService (dbfile string) FilePersistenceService {
    var fps FilePersistenceService
    db, err := gorm.Open(sqlite.Open(dbfile), &gorm.Config{})
    if err != nil {
      panic("failed to connect database")
    }
    fps.db = db
    return fps
}

type FilePersistenceService struct {
  db *gorm.DB
}

func (this *FilePersistenceService) AddFile (file File) {
  this.db.Create(&file)
}

func (this *FilePersistenceService) GetFile (fileId int) File{
  var file File
  this.db.Preload("Tags").First(&file, fileId)	
  return file
}

func (this *FilePersistenceService) UpdatePath (fileId int, filePath string) {
  this.db.Where(&File{ID: uint(fileId)}).Update("Path", filePath)
}

func (this *FilePersistenceService) GetTag (fileId int, tagName string) Tag{
  var tag Tag
  this.db.Where(&Tag{Name: tagName, FileRefer: uint(fileId)}).First(&tag)
  return tag
}

func (this *FilePersistenceService) AddTag (fileId int, tagName, tagValue string) {
  var file File
  this.db.First(&file, fileId)
  file.Tags = append(file.Tags, Tag{Name: tagName, Value: tagValue})
  this.db.Model(&file).Update("Tags", file.Tags)
}

func (this *FilePersistenceService) UpdateTag (fileId int, tagName, tagValue string) {
  this.db.Where(&Tag{Name: tagName, FileRefer: uint(fileId)}).Update("Value", tagValue)
}


func (this *FilePersistenceService) DeleteTag (fileId int, tagName string) {
  this.db.Where(&Tag{Name: tagName, FileRefer: uint(fileId)}).Delete(&Tag{})
}


func (this *FilePersistenceService) DeleteFile (fileId int) {
  this.db.Where(&File{ID: uint(fileId)}).Delete(&File{})
}

func (this *FilePersistenceService) FindAll () []File{
  var files []File
  this.db.Preload("Tags").Find(&files)
  return files
}


func (this *FilePersistenceService) FindByPath (filePath string) []File{
  var files []File
  this.db.Preload("Tags").Where(&File{Path: filePath}).Find(&files)
  return files
}

func (this *FilePersistenceService) FindByTagName (tagName string) []File{
  var files []File
  var tags []Tag
  this.db.Where(&Tag{Name: tagName}).Find(&tags)
  if len(tags) > 0 {
    ids := reduceTagIds(tags)
    this.db.Where(ids).Preload("Tags").Find(&files)
  }
  return files
}

func (this *FilePersistenceService) FindByTagValue (tagName, tagValue string) []File{
  var files []File
	var tags []Tag
  this.db.Where(&Tag{Name: tagName, Value: tagValue}).Find(&tags)
  if len(tags) > 0 {
    ids := reduceTagIds(tags)
    this.db.Where(ids).Preload("Tags").Find(&files)
  }
  return files
}