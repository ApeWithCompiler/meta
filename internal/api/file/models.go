package file

import (
	"gorm.io/gorm"
)

type File struct {
  gorm.Model
  ID      uint `gorm:"primaryKey"`
  Path    string
  Tags []Tag `gorm:"foreignKey:FileRefer"`
}

type Tag struct {
  gorm.Model
  Name    string
  Value    string
  FileRefer uint
}