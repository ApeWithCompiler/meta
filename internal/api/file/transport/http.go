package transport

import (
  "net/http"
  "io/ioutil"
  "encoding/json"
  "strconv"
  "github.com/gorilla/mux"
)

type Tag struct {
	Name string `json:"name"`
	Value string	`json:"value"`
}

type File struct {
	Id uint `json:"id"`
	Path string `json:"path"`
	Tags []Tag `json:"tags"`
}

type TagUpdate struct {
	Value string	`json:"value"`
}

type PathUpdate struct {
	Path string	`json:"value"`
}

type TagListResponse struct {
	Code int `json:"code"`
	Tags []Tag `json:"tags"`
}

type FileListResponse struct {
	Code int `json:"code"`
	Files []File `json:"files"`
}

type ErrorResponse struct {
	Code int `json:"code"`
	Message string `json:"message"`
}

func ParseFileId (r *http.Request) int {
    parameters := mux.Vars(r)
    fileId, _ := strconv.Atoi(parameters["fileId"])
    return fileId 
}

func ParseTagName (r *http.Request) string {
    parameters := mux.Vars(r)
    return parameters["tagName"]
}

func ParseQueryTagName (r *http.Request) string {
    return r.URL.Query().Get("tagName")
}

func ParseQueryTagValue (r *http.Request) string {
    return r.URL.Query().Get("tagValue")
}

func ParseQueryPath (r *http.Request) string {
    return r.URL.Query().Get("path")
}

func ParseFileBody (r *http.Request) File {
    reqBody, _ := ioutil.ReadAll(r.Body)
    var file File
    json.Unmarshal(reqBody, &file)
    return file    
}

func ParseTagBody (r *http.Request) Tag {
    reqBody, _ := ioutil.ReadAll(r.Body)
    var tag Tag
    json.Unmarshal(reqBody, &tag)
    return tag    
}

func ParseTagUpdateBody (r *http.Request) TagUpdate {
    reqBody, _ := ioutil.ReadAll(r.Body)
    var tag TagUpdate
    json.Unmarshal(reqBody, &tag)
    return tag    
}

func ParsePathUpdateBody (r *http.Request) PathUpdate {
    reqBody, _ := ioutil.ReadAll(r.Body)
    var path PathUpdate
    json.Unmarshal(reqBody, &path)
    return path    
}

func setResponseHeader(w http.ResponseWriter, statusCode int) http.ResponseWriter {
  w.Header().Add("Content-Type", "application/json")
  w.WriteHeader(statusCode)
  return w	
}

func SendFileListResponse (w http.ResponseWriter, files []File) {
	w = setResponseHeader(w, http.StatusOK)

	var response FileListResponse
  response.Files = files
  response.Code = http.StatusOK

  json.NewEncoder(w).Encode(response)	
}

func SendFileCreatedResponse (w http.ResponseWriter, files []File) {
	w = setResponseHeader(w, http.StatusCreated)

	var response FileListResponse
  response.Files = files
  response.Code = http.StatusCreated

  json.NewEncoder(w).Encode(response)	
}

func SendTagListResponse (w http.ResponseWriter, tags []Tag) {
	w = setResponseHeader(w, http.StatusOK)

	var response TagListResponse
  response.Tags = tags
  response.Code = http.StatusOK

  json.NewEncoder(w).Encode(response)	
}

func SendNotFoundResponse (w http.ResponseWriter, message string, id int) {
  w = setResponseHeader(w, http.StatusNotFound)

	var response ErrorResponse
  response.Code = http.StatusNotFound
  response.Message = message

  json.NewEncoder(w).Encode(response)	
}