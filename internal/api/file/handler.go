package file

import (
  "net/http"
  "gitlab.com/ApeWithCompiler/meta/internal/api/file/transport"
)

func convertHandlerToEngineTag (handlerTag transport.Tag) Tag {
    var fileTag Tag
    fileTag.Name = handlerTag.Name
    fileTag.Value = handlerTag.Value

    return fileTag
}

func convertEngineToHandlerTag (fileTag Tag) transport.Tag {
    var handlerTag transport.Tag
    handlerTag.Name = fileTag.Name
    handlerTag.Value = fileTag.Value

    return handlerTag
}

func convertEngineToHandlerTags (fileTags []Tag) []transport.Tag {
    var handlerTags []transport.Tag
    for i,_ := range(fileTags) {
        handlerTag := convertEngineToHandlerTag(fileTags[i])
        handlerTags = append(handlerTags, handlerTag)
    }

    return handlerTags
}

func convertHandlerToEngineFile (handlerFile transport.File) File {
    var fileFile File
    fileFile.Path = handlerFile.Path
    for i,_ := range(handlerFile.Tags) {
        fileTag := convertHandlerToEngineTag(handlerFile.Tags[i])
        fileFile.Tags = append(fileFile.Tags, fileTag)
    }

    return fileFile
}

func convertEngineToHandlerFile (fileFile File) transport.File {
    var handlerFile transport.File
    handlerFile.Id = fileFile.ID
    handlerFile.Path = fileFile.Path
    for i,_ := range(fileFile.Tags) {
        handlerTag := convertEngineToHandlerTag(fileFile.Tags[i])
        handlerFile.Tags = append(handlerFile.Tags, handlerTag)
    }
    
    return handlerFile
}

func convertEngineToHandlerFiles (fileFiles []File) []transport.File {
    var handlerFiles []transport.File
    for i,_ := range(fileFiles) {
        handlerFile := convertEngineToHandlerFile(fileFiles[i])
        handlerFiles = append(handlerFiles, handlerFile)
    }
    
    return handlerFiles
}

func CreateFileHandler (dbfile string) FileHandler {
    var handler FileHandler
    EnsureDbExists(dbfile)
    persistence := CreateFilePersistenceService(dbfile)
    handler.persistence = persistence
    return handler
}

type FileHandler struct {
    persistence FilePersistenceService
}

func (this *FileHandler) GetFilesList(w http.ResponseWriter, r *http.Request){
    queryTagName := transport.ParseQueryTagName(r)
    queryTagValue := transport.ParseQueryTagValue(r)
    queryPath := transport.ParseQueryPath(r)

    var files []File
    if queryPath != "" {
        files = this.persistence.FindByPath(queryPath)
    } else if queryTagName != "" && queryTagValue != "" {
        files = this.persistence.FindByTagValue(queryTagName, queryTagValue)
    } else if queryTagName != "" {
        files = this.persistence.FindByTagName(queryTagName)
    } else {
        files = this.persistence.FindAll()
    }

    transport.SendFileListResponse(w, convertEngineToHandlerFiles(files))
}

func (this *FileHandler) PostFiles(w http.ResponseWriter, r *http.Request){    
    fileBody := transport.ParseFileBody(r)
    file := convertHandlerToEngineFile(fileBody)
    this.persistence.AddFile(file)

    transport.SendFileCreatedResponse(w, convertEngineToHandlerFiles([]File{file}))
}

func (this *FileHandler) GetFile(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)

    file := this.persistence.GetFile(fileId)
    if file.ID != uint(fileId) {
        transport.SendNotFoundResponse(w, "File not found!", fileId)
    } else {
        transport.SendFileListResponse(w, convertEngineToHandlerFiles([]File{file}))
    }

}

func (this *FileHandler) DeleteFile(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)

    this.persistence.DeleteFile(fileId)

    transport.SendFileListResponse(w, convertEngineToHandlerFiles([]File{}))
}

func (this *FileHandler) PutFilePath(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    pathUpdateBody := transport.ParsePathUpdateBody(r)

    this.persistence.UpdatePath(fileId, pathUpdateBody.Path)

    transport.SendFileListResponse(w, convertEngineToHandlerFiles([]File{}))
}

func (this *FileHandler) GetFileTagsList(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    file := this.persistence.GetFile(fileId)

    transport.SendTagListResponse(w, convertEngineToHandlerTags(file.Tags))
}

func (this *FileHandler) PostFileTag(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    tagBody := transport.ParseTagBody(r)

    this.persistence.AddTag(fileId, tagBody.Name, tagBody.Value)
    
    transport.SendTagListResponse(w, convertEngineToHandlerTags([]Tag{}))
}

func (this *FileHandler) GetFileTag(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    tagName := transport.ParseTagName(r)

    tag := this.persistence.GetTag(fileId, tagName)
    if tag.Name != tagName {
        transport.SendNotFoundResponse(w, "Tag not found!", fileId)
    } else {
    transport.SendTagListResponse(w, convertEngineToHandlerTags([]Tag{tag}))
    }  
}

func (this *FileHandler) PutFileTagValue(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    tagName := transport.ParseTagName(r)
    tagUpdateBody := transport.ParseTagUpdateBody(r)

    this.persistence.UpdateTag(fileId, tagName, tagUpdateBody.Value)

    transport.SendTagListResponse(w, convertEngineToHandlerTags([]Tag{}))
}

func (this *FileHandler) DeleteFileTag(w http.ResponseWriter, r *http.Request){
    fileId := transport.ParseFileId(r)
    tagName := transport.ParseTagName(r)

    this.persistence.DeleteTag(fileId, tagName)

     transport.SendTagListResponse(w, convertEngineToHandlerTags([]Tag{}))
}
