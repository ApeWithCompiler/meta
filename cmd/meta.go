package main

import (
	"os"
  "gitlab.com/ApeWithCompiler/meta/internal/api"
)

func main() {
	api.Start()
	os.Exit(0)	
}