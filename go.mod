module gitlab.com/ApeWithCompiler/meta

go 1.14

require (
  github.com/gorilla/mux v1.8.0
  github.com/gorilla/handlers v1.5.1
  gorm.io/driver/sqlite v1.1.4
  gorm.io/gorm v1.21.3
)